"""dash URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from arrya import views

from django.contrib.auth.views import LoginView, LogoutView

admin.site.site_header = 'Polaris'

urlpatterns = [
    # url(r'^jet/', include('jet.urls', 'jet')),
    # url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^login/$', LoginView.as_view(template_name='arrya/login.html'), name="login"),
    url(r'^logout/$', LogoutView.as_view(template_name='arrya/logout.html'), name='logout'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^chat/$', views.chat, name='chat'),
    url(r'^polar/(?P<room_name>[^/]+)/$', views.room, name='room'),
    url(r'^auth/room/(?P<room_name>[^/]+)/$', views.auth_room, name='auth_room'),
    url(r'^user/profile/(?P<username>[^/]+)/$', views.user_profile, name='user_profile'),
    url(r'^register/', views.register, name='register'),
    url(r'^account_setting/$', views.account_setting, name='account_setting'),
    url('firebase-messaging-sw.js', views.ServiceWorkerView.as_view(), name='service_worker'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)