// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
importScripts('https://www.gstatic.com/firebasejs/6.2.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.2.4/firebase-messaging.js');


// Initialize Firebase
  var config = {
    apiKey: "AAAAkGOc1NQ:APA91bGZTugFwBibWJe30ErfqkdLbN6BOeX-IB3iHIqS8DN3LDZo9pe-U9TzQzGhLx_byuLw5H0HuDWn3ZEIxGPAVOJPq1bTNISBmP8Eha6kdwcpv6zklmYQWn5JssjTzqENk_kLF-VG",
    authDomain: "<PROJECT-ID>.firebaseapp.com",
    databaseURL: "https://lintang-971d1.firebaseio.com",
    projectId: "lintang-971d1",
    storageBucket: "lintang-971d1.appspot.com",
    messagingSenderId: "AAAAkGOc1NQ:APA91bGZTugFwBibWJe30ErfqkdLbN6BOeX-IB3iHIqS8DN3LDZo9pe-U9TzQzGhLx_byuLw5H0HuDWn3ZEIxGPAVOJPq1bTNISBmP8Eha6kdwcpv6zklmYQWn5JssjTzqENk_kLF-VG"
  };
  firebase.initializeApp(config);


var messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Lintang has send a notification';
  var notificationOptions = {
    body: 'Message',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});