import datetime
import random
import re

import firebase_admin

import online_users.models

from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect

from django.contrib.auth.decorators import login_required

from django.urls import reverse

from django.shortcuts import render, redirect

from django.views import View

from arrya.models import AppUser, AppSettings, ThreadAppUser

from django.conf import settings

from firebase_admin import credentials

from firebase_admin import messaging

from datetime import timedelta

from django.contrib.auth.models import User

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth import update_session_auth_hash

from django.contrib.auth.hashers import make_password


# Create your views here.

def register(request):
    message = """
                Username Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.<br>
                Your password can't be too similar to your other personal information.<br>
                Your password must contain at least 8 characters.<br>
                Your password can't be a commonly used password.<br>
                Your password can't be entirely numeric.<br>
            """

    if request.method == 'POST':
        username = request.POST.get('username')
        raw_password = request.POST.get('password1')
        raw_password2 = request.POST.get('password2')
        first_name = request.POST.get('firstname')
        last_name = request.POST.get('lastname')
        gender = request.POST.get('gender')

        # auths = authenticate(username=username, password=raw_password)

        user = User(username=username, password=make_password(raw_password), first_name=first_name, last_name=last_name, is_staff=True)

        if raw_password == raw_password2:
            try:
                user.save()

                profile = AppUser(user=user)
                profile.save()

                login(request, user)

                return redirect('dashboard')
            except:
                message = "Failed to create account"
        else:
            message = "Password not same with Re-type Password column"

    context = {
        "message": message,
    }

    return render(request, 'arrya/register.html', context)