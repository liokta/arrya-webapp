import datetime
import random
import re
import json

import requests

import firebase_admin

import online_users.models

import secrets

from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect

from django.contrib.auth.decorators import login_required

from django.urls import reverse

from django.shortcuts import render, redirect

from django.views import View

from arrya.models import AppUser, AppSettings, ThreadAppUser, ChatRoom, RoomSession, RoomHistory, UserRoomHistory

from django.conf import settings

from firebase_admin import credentials

from firebase_admin import messaging

from datetime import timedelta

from django.utils import timezone

from lintang.calendar import Neptu

from django.contrib.auth.models import User

from django.utils.safestring import mark_safe
import json


cred = credentials.Certificate(settings.FIREBASE_SERVIVE_ACCOUNT)
firebase_cred = firebase_admin.initialize_app(cred)


# Create your views here.

@login_required(login_url='login/')
def dashboard(request):

    app_name = AppSettings.objects.filter(code='app.name').first()
    app_version = AppSettings.objects.filter(code='app.version').first()

    threads = ThreadAppUser.objects.all().order_by('-created_date')[:15]

    profile = AppUser.objects.filter(user=request.user).first();

    members = AppUser.objects.all()

    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    online_user = (user for user in user_status)

    onlines = []

    for o in online_user:
        onlines.append(o.user)

    profile_online = AppUser.objects.filter(user__in=onlines)

    now = timezone.now()

    day = now.day

    month = now.month

    year = now.year

    javanese = Neptu.convert_date(day,month,year)


    context = {
        "app_name": app_name.value,
        "app_version": app_version.value,
        "threads": threads,
        "user": request.user,
        "profile": profile,
        "online_user": profile_online,
        "members": members,
        "javanese_day": "{}, {}".format(javanese.get('day_name'), javanese.get('pasaran')),
        "javanese_month": "{} {}".format(javanese.get('month_name'), javanese.get('day')),
        "failed_message": ""
    }


    if request.POST.get('firebase_id'):
        user = AppUser.objects.filter(user=request.user).first()

        user.firebase_id = request.POST.get('firebase_id')

        user.save()

        return redirect(reverse('dashboard'))

    if request.POST.get('thread'):

        status = ThreadAppUser(app_user=profile, thread=request.POST.get('thread'))

        status.save()

        return redirect(reverse('dashboard'))



    if request.POST.get('profile'):

        bio = User.objects.filter(id=request.user.id).first()
        bio.first_name = request.POST.get('firstname')
        bio.last_name = request.POST.get('lastname')
        bio.email = request.POST.get('email')

        bio.save()

        return redirect(reverse('dashboard'))


    if request.POST.get('greetings'):

        profile.greeting = request.POST.get('content')
        profile.save()

        return redirect(reverse('dashboard'))


    if request.POST.get('change_password'):

        pass1 = request.POST.get('password1')
        pass2 = request.POST.get('password2')

        if pass1==pass2:
            u = User.objects.get(username__exact=request.user.username)
            u.set_password(pass1)
            u.save()

            return redirect(reverse('dashboard'))
        else:
            context['failed_message'] = "Failed to change password!"
            return render(request, 'arrya/profile.html', context)




    if request.FILES.get('file'):
        profile.profile_picture = request.FILES.get('file')
        profile.save()
        return redirect(reverse('dashboard'))

    if request.GET.get('content') and request.GET.get('content') == 'desktop':
        return render(request, 'arrya/desktop_main.html', context)

    return render(request, 'arrya/profile.html', context)


@login_required(login_url='login/')
def chat(request):

    app_name = AppSettings.objects.filter(code='app.name').first()
    app_version = AppSettings.objects.filter(code='app.version').first()

    threads = ThreadAppUser.objects.all().order_by('-created_date')[:15]

    profile = AppUser.objects.filter(user=request.user).first();

    members = AppUser.objects.all()

    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    online_user = (user for user in user_status)

    onlines = []

    for o in online_user:
        onlines.append(o.user)

    profile_online = AppUser.objects.filter(user__in=onlines)

    now = timezone.now()

    day = now.day

    month = now.month

    year = now.year

    javanese = Neptu.convert_date(day,month,year)

    room = ChatRoom.objects.filter(is_public=True)

    private_room = ChatRoom.objects.filter(app_user=profile, is_public=False)

    room_available = ChatRoom.objects.filter(is_public=False)

    session = RoomSession.objects.filter(app_user=profile)

    session.delete()


    context = {
        "app_name": app_name.value,
        "app_version": app_version.value,
        "threads": threads,
        "user": request.user,
        "profile": profile,
        "online_user": profile_online,
        "members": members,
        "javanese_day": "{}, {}".format(javanese.get('day_name'), javanese.get('pasaran')),
        "javanese_month": "{} {}".format(javanese.get('month_name'), javanese.get('day')),
        "public_room": room,
        "private_room": private_room,
        "room_available": room_available,
        "message": ""
    }


    if request.POST.get('create_room'):

        is_available = ChatRoom.objects.filter(name=request.POST.get('room_name')).first()

        if not is_available:
            new_room = ChatRoom(app_user=profile, name=request.POST.get('room_name'), key=request.POST.get('room_key'), is_public=False)
            new_room.save()

            if request.GET.get('content') and request.GET.get('content') == 'desktop':
                return redirect('{}?content=desktop'.format(reverse('chat')))

            return redirect(reverse('chat'))

        else:
            context['message'] = "Failed to created room because room name has already used"
            if request.GET.get('content') and request.GET.get('content') == 'desktop':
                return render(request, 'arrya/chat_desktop.html', context)
            return render(request, 'arrya/polar.html', context)


    if request.GET.get('content') and request.GET.get('content') == 'desktop':
        return render(request, 'arrya/chat_desktop.html', context)


    return render(request, 'arrya/polar.html', context)







@login_required(login_url='login/')
def auth_room(request, room_name):
    app_name = AppSettings.objects.filter(code='app.name').first()
    app_version = AppSettings.objects.filter(code='app.version').first()

    threads = ThreadAppUser.objects.all().order_by('-created_date')[:15]

    profile = AppUser.objects.filter(user=request.user).first();

    members = AppUser.objects.all()

    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    online_user = (user for user in user_status)

    onlines = []

    for o in online_user:
        onlines.append(o.user)

    profile_online = AppUser.objects.filter(user__in=onlines)

    now = timezone.now()

    day = now.day

    month = now.month

    year = now.year

    javanese = Neptu.convert_date(day, month, year)

    room = ChatRoom.objects.filter(is_public=True)

    private_room = ChatRoom.objects.filter(app_user=profile, is_public=False)

    room_available = ChatRoom.objects.filter(is_public=False)

    context = {
        "app_name": app_name.value,
        "app_version": app_version.value,
        "threads": threads,
        "user": request.user,
        "profile": profile,
        "online_user": profile_online,
        "members": members,
        "javanese_day": "{}, {}".format(javanese.get('day_name'), javanese.get('pasaran')),
        "javanese_month": "{} {}".format(javanese.get('month_name'), javanese.get('day')),
        "public_room": room,
        "private_room": private_room,
        "room_available": room_available,
        "message": "",
        "room_name": room_name,
    }


    if request.POST.get('auth_room'):

        authentication_room = ChatRoom.objects.filter(name=room_name, key=request.POST.get('key')).first()

        if not authentication_room:

            context['message'] = "Wrong room key"
            return render(request, 'arrya/auth_room.html', context)

        else:
            token = secrets.token_hex(64)

            new_session = RoomSession(app_user=profile, room=authentication_room, token=token)
            new_session.save()

            return redirect(reverse('room', kwargs={'room_name': room_name})+"?token={}".format(new_session.token))



    return render(request, 'arrya/auth_room.html', context)






@login_required(login_url='../login')
def room(request, room_name):


    public = ChatRoom.objects.filter(name=room_name).first()

    if public.is_public==False:

        if not request.GET.get('token'):
            return redirect(reverse('auth_room', kwargs={'room_name': room_name}))

        auth = RoomSession.objects.filter(token=request.GET.get('token')).first()

        if not auth or auth.is_expired==True:

            return redirect(reverse('auth_room', kwargs={'room_name':room_name}))

    app_name = AppSettings.objects.filter(code='app.name').first()
    app_version = AppSettings.objects.filter(code='app.version').first()

    threads = ThreadAppUser.objects.all().order_by('-created_date')[:15]

    profile = AppUser.objects.filter(user=request.user).first();

    members = AppUser.objects.all()

    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    online_user = (user for user in user_status)

    onlines = []

    for o in online_user:
        onlines.append(o.user)

    profile_online = AppUser.objects.filter(user__in=onlines)

    now = timezone.now()

    day = now.day

    month = now.month

    year = now.year

    javanese = Neptu.convert_date(day, month, year)

    room_log = RoomHistory.objects.filter(room=public).first()

    last_history = UserRoomHistory.objects.filter(room=public)

    context = {
        "app_name": app_name.value,
        "app_version": app_version.value,
        "threads": threads,
        "user": request.user,
        "profile": profile,
        "online_user": profile_online,
        "members": members,
        "javanese_day": "{}, {}".format(javanese.get('day_name'), javanese.get('pasaran')),
        "javanese_month": "{} {}".format(javanese.get('month_name'), javanese.get('day')),
        "room_name_json": mark_safe(json.dumps(room_name)),
        'room_name': room_name,
        'room': public,
        'room_log': '' if not room_log else room_log.history,
        'last_history': last_history,
    }

    if request.POST.get('log'):

        logs = ChatRoom.objects.filter(name=room_name).first()

        hist = request.POST.get('log')

        history = RoomHistory.objects.filter(room=logs).first()
        if history:
            history.history = hist
            history.save()
        else:
            history = RoomHistory(room=logs, history=hist)
            history.save()

        if request.POST.get('message'):
            pass
            session_chat = '' if not request.POST.get('session') else request.POST.get('session')
            new_user_log = UserRoomHistory(room=logs, app_user=profile, message=request.POST.get('message'), session=session_chat)
            new_user_log.save()

        return render(request, 'arrya/nroom.html', context)

    return render(request, 'arrya/nroom.html', context)






@login_required(login_url='login/')
def user_profile(request, username):

    app_name = AppSettings.objects.filter(code='app.name').first()
    app_version = AppSettings.objects.filter(code='app.version').first()

    threads = ThreadAppUser.objects.all().order_by('-created_date')[:15]

    userprofile = User.objects.filter(username=username).first()

    profile = AppUser.objects.filter(user=userprofile).first();

    members = AppUser.objects.all()

    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    online_user = (user for user in user_status)

    onlines = []

    for o in online_user:
        onlines.append(o.user)

    profile_online = AppUser.objects.filter(user__in=onlines)

    now = timezone.now()

    day = now.day

    month = now.month

    year = now.year

    javanese = Neptu.convert_date(day,month,year)


    context = {
        "app_name": app_name.value,
        "app_version": app_version.value,
        "threads": threads,
        "user": userprofile,
        "profile": profile,
        "online_user": profile_online,
        "members": members,
        "javanese_day": "{}, {}".format(javanese.get('day_name'), javanese.get('pasaran')),
        "javanese_month": "{} {}".format(javanese.get('month_name'), javanese.get('day')),
    }


    return render(request, 'arrya/user_profile.html', context)




@login_required(login_url='login/')
def account_setting(request):

    app_name = AppSettings.objects.filter(code='app.name').first()
    app_version = AppSettings.objects.filter(code='app.version').first()

    threads = ThreadAppUser.objects.all().order_by('-created_date')[:15]

    profile = AppUser.objects.filter(user=request.user).first();

    members = AppUser.objects.all()

    user_status = online_users.models.OnlineUserActivity.get_user_activities(timedelta(seconds=60))
    online_user = (user for user in user_status)

    onlines = []

    for o in online_user:
        onlines.append(o.user)

    profile_online = AppUser.objects.filter(user__in=onlines)

    now = timezone.now()

    day = now.day

    month = now.month

    year = now.year

    javanese = Neptu.convert_date(day,month,year)


    context = {
        "app_name": app_name.value,
        "app_version": app_version.value,
        "threads": threads,
        "user": request.user,
        "profile": profile,
        "online_user": profile_online,
        "members": members,
        "javanese_day": "{}, {}".format(javanese.get('day_name'), javanese.get('pasaran')),
        "javanese_month": "{} {}".format(javanese.get('month_name'), javanese.get('day')),
        "failed_message": ""
    }


    if request.POST.get('firebase_id'):
        user = AppUser.objects.filter(user=request.user).first()

        user.firebase_id = request.POST.get('firebase_id')

        user.save()

        return redirect('{}?content=desktop'.format(reverse('dashboard')))

    if request.POST.get('thread'):

        status = ThreadAppUser(app_user=profile, thread=request.POST.get('thread'))

        status.save()

        return redirect('{}?content=desktop'.format(reverse('dashboard')))



    if request.POST.get('profile'):

        bio = User.objects.filter(id=request.user.id).first()
        bio.first_name = request.POST.get('firstname')
        bio.last_name = request.POST.get('lastname')
        bio.email = request.POST.get('email')

        bio.save()

        return redirect('{}?content=desktop'.format(reverse('dashboard')))


    if request.POST.get('greetings'):

        profile.greeting = request.POST.get('content')
        profile.save()

        return redirect('{}?content=desktop'.format(reverse('dashboard')))


    if request.POST.get('change_password'):

        pass1 = request.POST.get('password1')
        pass2 = request.POST.get('password2')

        if pass1==pass2:
            u = User.objects.get(username__exact=request.user.username)
            u.set_password(pass1)
            u.save()

            return redirect('{}?content=desktop'.format(reverse('dashboard')))
        else:
            context['failed_message'] = "Failed to change password!"
            return render(request, 'arrya/account_setting.html', context)




    if request.FILES.get('file'):
        profile.profile_picture = request.FILES.get('file')
        profile.save()
        return redirect('{}?content=desktop'.format(reverse('dashboard')))

    return render(request, 'arrya/account_setting.html', context)


class ServiceWorkerView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'arrya/firebase-messaging-sw.js', content_type="application/x-javascript")
