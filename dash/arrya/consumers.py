from channels.generic.websocket import AsyncWebsocketConsumer
import json

from asgiref.sync import async_to_sync

from arrya.models import AppUser, AppSettings, ThreadAppUser, ChatRoom, RoomSession, RoomHistory

from channels.auth import login

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user = self.scope["user"]
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name,
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        room_id = text_data_json['room']
        room_history = text_data_json['history']
        photo = text_data_json['photo']

        if message and message is not '':

        # Send message to room group
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': '{}: {}'.format(self.user, message),
                    'actMessage': '{}'.format(message),
                    'user': '{}'.format(self.user),
                    'photo': '{}'.format(photo),
                }
            )


    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        actMessage = event['actMessage']
        user = event['user']
        photo = event['photo']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'actMessage': '{}'.format(actMessage),
            'user': '{}'.format(user),
            'img': '{}'.format(photo),
        }))
