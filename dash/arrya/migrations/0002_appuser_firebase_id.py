# Generated by Django 2.2.3 on 2019-07-10 09:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('arrya', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='appuser',
            name='firebase_id',
            field=models.TextField(blank=True, null=True),
        ),
    ]
