# Generated by Django 2.2.3 on 2019-07-25 12:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('arrya', '0012_userroomhistory'),
    ]

    operations = [
        migrations.AddField(
            model_name='userroomhistory',
            name='session',
            field=models.TextField(blank=True, null=True),
        ),
    ]
