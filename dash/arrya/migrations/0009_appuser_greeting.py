# Generated by Django 2.2.3 on 2019-07-24 12:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('arrya', '0008_chatroom'),
    ]

    operations = [
        migrations.AddField(
            model_name='appuser',
            name='greeting',
            field=models.TextField(blank=True, null=True),
        ),
    ]
