import uuid
import re

from django.db import models
from sorl.thumbnail import ImageField

from django.utils import timezone, dateparse

from django.conf import settings
from django.contrib.auth.models import AbstractUser

from django.utils.translation import ugettext_lazy as _


class AppUser(models.Model):

    MALE = 0
    FEMALE = 1

    GENDER = (
        (MALE, 'Male'),
        (FEMALE, 'Female')
    )

    # add additional fields in here
    id              = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user            = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)
    gender          = models.PositiveIntegerField(choices=GENDER, default=MALE)
    firebase_id     = models.TextField(null=True, blank=True,)
    profile_picture = ImageField(upload_to='user/profile', null=True, blank=True, max_length=225,)
    background      = ImageField(upload_to='user/background', null=True, blank=True, max_length=225, )
    is_online       = models.BooleanField(default=False)
    greeting        = models.TextField(null=True, blank=True, )

    def __str__(self):
        return "{}".format(self.user.username)

    class Meta:
        db_table = 'app_users'



class ChatRoom(models.Model):
    # add additional fields in here
    id              = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    app_user        = models.ForeignKey('AppUser', blank=True, null=True, on_delete = models.CASCADE)
    name            = models.CharField(max_length=225, null=True, blank=True,)
    key             = models.CharField(max_length=225, null=True, blank=True, )
    is_public       = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        db_table = 'chat_rooms'



class RoomHistory(models.Model):
    # add additional fields in here
    id              = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    room            = models.ForeignKey('ChatRoom', blank=True, null=True, on_delete=models.CASCADE)
    history         = models.TextField(null=True, blank=True, )
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}".format(self.room.name)

    class Meta:
        db_table = 'room_histories'


class UserRoomHistory(models.Model):
    # add additional fields in here
    id              = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    app_user        = models.ForeignKey('AppUser', blank=True, null=True, on_delete=models.CASCADE)
    room            = models.ForeignKey('ChatRoom', blank=True, null=True, on_delete=models.CASCADE)
    message         = models.TextField(null=True, blank=True, )
    session         = models.TextField(null=True, blank=True, )
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}".format(self.room.name)

    class Meta:
        db_table = 'user_room_histories'



class RoomSession(models.Model):
    # add additional fields in here
    id              = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    app_user        = models.ForeignKey('AppUser', blank=True, null=True, on_delete = models.CASCADE)
    room            = models.ForeignKey('ChatRoom', blank=True, null=True, on_delete=models.CASCADE)
    token           = models.TextField(null=True, blank=True, )
    is_expired      = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'room_sessions'



class AppSettings(models.Model):
    # add additional fields in here
    id              = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    code            = models.CharField(max_length=32, null=True, blank=True,)
    value           = models.CharField(max_length=225, null=True, blank=True,)
    created_date    = models.DateTimeField(auto_now_add=True)
    updated_date    = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'app_settings'
        verbose_name = 'Setting'




class ThreadAppUser(models.Model):
    # add additional fields in here
    id              = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    app_user        = models.ForeignKey('AppUser', on_delete=models.CASCADE, blank=True, null=True)
    thread          = models.TextField(null=True, blank=True,)
    created_date    = models.DateTimeField(auto_now_add=True)
    updated_date    = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'thread_app_users'

