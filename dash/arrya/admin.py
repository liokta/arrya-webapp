from django.contrib import admin
from arrya.models import AppUser, AppSettings, ThreadAppUser, ChatRoom, RoomSession, RoomHistory, UserRoomHistory

# Register your models here.

class AppUserAdmin(admin.ModelAdmin):
    list_display = ('user',)


admin.site.register(AppUser, AppUserAdmin)



class AppSettingsAdmin(admin.ModelAdmin):
    list_display = ('code', 'value')


admin.site.register(AppSettings, AppSettingsAdmin)



class ThreadAppUserAdmin(admin.ModelAdmin):
    list_display = ('app_user', 'created_date')


admin.site.register(ThreadAppUser, ThreadAppUserAdmin)


class ChatRoomAdmin(admin.ModelAdmin):
    list_display = ('app_user', 'name')


admin.site.register(ChatRoom, ChatRoomAdmin)


class RoomSessionAdmin(admin.ModelAdmin):
    list_display = ('app_user', 'room', 'token')


admin.site.register(RoomSession, RoomSessionAdmin)



class RoomHistoryAdmin(admin.ModelAdmin):
    list_display = ('room',)


admin.site.register(RoomHistory, RoomHistoryAdmin)


class UserRoomHistoryAdmin(admin.ModelAdmin):
    list_display = ('room', 'app_user', 'message',)


admin.site.register(UserRoomHistory, UserRoomHistoryAdmin)